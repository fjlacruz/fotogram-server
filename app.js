"use strict";
var express = require("express");
var bodyParser = require("body-parser");
var cors = require("cors");
var app = express();

//============================= carga archivos de rutas =============================//
var project_routes = require("./routes/endpoint");

//============================== middlewares ========================================//
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

//================== Configurar cabeceras y cors ====================================//
app.use(cors());
app.use(function (req, res, next) {
  //Enabling CORS
  res.header("Access-Control-Allow-Origin", "*"); // restrict it to the required domain
  res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE,OPTIONS");
  // Set custom headers for CORS
  res.header(
    "Access-Control-Allow-Headers",
    "Content-type,Accept,X-Custom-Header"
  );
  //res.header("Access-Control-Allow-Credentials", true);
  next();
});

//============================= Llamado a las rutas ==================================//
app.use("/api", project_routes);

// exportar
module.exports = app;
