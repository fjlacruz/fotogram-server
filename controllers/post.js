"use strict";

var Post = require("../models/post");
var fs = require("fs");
var path = require("path");
const post = require("../models/post");

var controller = {
  //================ Guardar Posts ============================//
  savePost: function (req, res) {
    var post = new Post();

    var params = req.body;
    post.mensaje = req.body.mensaje;
    post.coords = req.body.coords;
    post.usuario = req.user.sub;

    post.save((err, postStored) => {
      if (err)
        return res
          .status(500)
          .send({ message: "Error al guardar el documento." });

      if (!postStored)
        return res
          .status(404)
          .send({ message: "No se ha podido guardar el post." });

      return res
        .status(200)
        .send({ resp: { post: postStored, usuario: req.user } });
    });
  },
  //================== Obtiene lista de posts ================================//
  getListPosts: function (req, res) {
    //======== paginacion de los resultados ======//
    let pagina = Number(req.query.pagina) || 1;
    let skip = pagina - 1;
    skip = skip * 10;

    Post.find({})
      .sort({ _id: -1 })
      .populate("usuario", "-password")
      .limit(10)
      .skip(skip)
      .exec((err, posts) => {
        if (err)
          return res
            .status(500)
            .send({ message: "Error al devolver los datos." });

        if (!posts)
          return res
            .status(404)
            .send({ message: "No hay projectos que mostrar." });

        return res.status(200).send({ posts: posts, pagina: pagina });
      });
  },
  //============== Obtiene lista de posts por usuario ======================//
  getUsersPosts: function (req, res) {
    var usId = req.user.sub;

    let pagina = Number(req.query.pagina) || 1;
    let skip = pagina - 1;
    skip = skip * 10;

    //console.log(pagina);

    Post.find({ usuario: usId })
      .sort({ _id: -1 })
      .populate("usuario", "-password")
      .limit(10)
      .skip(skip)
      .exec((err, posts) => {
        if (err)
          return res
            .status(500)
            .send({ message: "Error al devolver los datos." });

        if (!posts)
          return res
            .status(404)
            .send({ message: "No hay projectos que mostrar." });

        return res.status(200).send({ posts: posts, pagina: pagina });
      });
  },

  //================== Obtiene la imagen del post ==================================//
  getPostImag: function (req, res) {
    var img = req.params.img;
    var usId = req.user.sub;
    var path_file = "./uploads/" + img;

    fs.exists("./uploads/" + img, function (exists) {
      if (exists) {
        return res.sendFile(path.resolve(path_file));
      } else {
        return res.status(200).send({ message: "No existe la imagen....." });
      }
    });
  },

  getProjects: function (req, res) {
    Project.find({})
      .sort("-year")
      .exec((err, projects) => {
        if (err)
          return res
            .status(500)
            .send({ message: "Error al devolver los datos." });

        if (!projects)
          return res
            .status(404)
            .send({ message: "No hay projectos que mostrar." });

        return res.status(200).send({ projects });
      });
  },

  updateProject: function (req, res) {
    var projectId = req.params.id;
    var update = req.body;

    Project.findByIdAndUpdate(
      projectId,
      update,
      { new: true },
      (err, projectUpdated) => {
        if (err)
          return res.status(500).send({ message: "Error al actualizar" });

        if (!projectUpdated)
          return res
            .status(404)
            .send({ message: "No existe el proyecto para actualizar" });

        return res.status(200).send({
          project: projectUpdated,
        });
      }
    );
  },

  deleteProject: function (req, res) {
    var projectId = req.params.id;

    Project.findByIdAndRemove(projectId, (err, projectRemoved) => {
      if (err)
        return res
          .status(500)
          .send({ message: "No se ha podido borrar el proyecto" });

      if (!projectRemoved)
        return res
          .status(404)
          .send({ message: "No se puede eliminar ese proyecto." });

      return res.status(200).send({
        project: projectRemoved,
      });
    });
  },

  buscar: function (req, res) {
    var langs = req.body.langs;
    console.log(langs);

    Project.find({ langs: langs })
      .sort("-year")
      .exec((err, projects) => {
        if (err)
          return res
            .status(500)
            .send({ message: "Error al devolver los datos." });

        if (!projects)
          return res
            .status(404)
            .send({ message: "No hay projectos que mostrar." });

        return res.status(200).send({ projects });
      });
  },
};

module.exports = controller;
