"use strict";

var User = require("../models/user");
var bcrypt = require("bcrypt-nodejs");
var jwt = require("../services/jwt.js");
var fs = require("fs");
var path = require("path");

var controller = {
  //============================================================================//
  //======================== crear usuarios ===================================//
  //============================================================================//
  saveUser: function (req, res) {
    var user = new User();

    var params = req.body;
    user.nombre = params.nombre;
    user.email = params.email;
    user.password = params.password;

    //valida que no exista el email
    User.count({ email: user.email }, function (err, count) {
      if (count >= 1) {
        return res.status(200).send({ message: "El correo ya se encuentra registrado." });
      } else {
        if (user.password) {
          //encriptar contraseña y guardar los datos
          bcrypt.hash(user.password, null, null, function (err, hash) {
            user.password = hash;
            if (
              user.nombre != null &&
              user.email != null &&
              user.password != null
            ) {
              //guarda el usuario
              user.save((err, userStore) => {
                if (err) {
                  res.status(500).send({ message: "Error al guardar....", ok: false });
                } else {
                  if (!userStore) {
                    res
                      .status(404)
                      .send({ message: "No se ha registrado el usuario", ok: false });
                  } else {
                    res.status(201).send({ token: jwt.generaToken(user), ok: true });
                  }
                }
              });
            } else {
              res.status(200).send({ message: "Introduce todos los campos" });
            }
          });
        } else {
          res.status(500).send({ message: "Debe introducir cotraseña...." });
        }
      }
    });
  },
  //============================================================================//
  //======================== Login de usuarios =================================//
  //============================================================================//
  loginUser: function (req, res) {
    var params = req.body;
    var email = params.email;
    var password = params.password;

    User.findOne({ email: email.toLowerCase() }, (err, user) => {
      if (err) {
        res.status(500).send({ message: "Error en la peticion....", token: null, ok: false });
      } else {
        if (!user) {
          res.status(201).send({ message: "El usuario no existe", token: null, ok: false });
        } else {
          //Comprobamos conntraseña
          bcrypt.compare(password, user.password, function (err, check) {
            if (check) {
              //devolver los datos del usuario loguaedo
              res.status(201).send({ token: jwt.generaToken(user), ok: true });
            } else {
              res
                .status(201)
                .send({ message: "El usuario no ha podido loguearse", ok: false, token: null });
            }
          });
        }
      }
    });
  },

  test: function (req, res) {
    console.log(req.user);
    res.status(200).send({ message: "test de modleware" });
  },

  //============================================================================//
  //===================== Obtiene los datos del usuario ========================//
  //============================================================================//
  getUser: function (req, res) {
    var user = req.user; //viene del Middleware de autenticacion
    //console.log(user);
    res.status(200).send({ user, ok: true });
  },
  //============================================================================//
  //========================= Actualizacion de usuarios ========================//
  //============================================================================//
  updateUser: function (req, res) {
    var userId = req.user.sub; //viene del Middleware de autenticacion
    var update = req.body;

    User.findByIdAndUpdate(userId, update, { new: true }, (err, userUpdate) => {
      if (err) {
        res.status(500).send({ message: "Error al actualizar el usuario", ok: false });
      } else {
        if (!userUpdate) {
          res
            .status(201)
            .send({ message: "No se ha podido actualizar el usuario", ok: false });
        } else {
          res
            .status(200)
            .send({ token: jwt.generaToken(update), ok: true });
        }
      }
    });
  },
  //============================================================================//
  //========================== listado de usuarios ============================//
  //===========================================================================//
  getUsers: function (req, res) {
    User.find({}).exec((err, user) => {
      if (err)
        return res
          .status(500)
          .send({ message: "Error al devolver los datos." });

      if (!user)
        return res
          .status(404)
          .send({ message: "No hay usuarios que mostrar." });

      return res.status(200).send({ user });
    });
  },

  //==========================================================================//
  //========================== Buscar de usuario =============================//
  //==========================================================================//
  searchUser: function (req, res) {
    var email = req.body.email;

    User.find({ email: email }).exec((err, user) => {
      if (err)
        return res
          .status(500)
          .send({ message: "Error al devolver los datos." });

      if (user == null || user == "")
        return res.status(200).send({ message: "No existe el usuario." });

      return res.status(200).send({ user: user });
    });

  },

  //==========================================================================//
  //====================== Subir imagen de usuario ===========================//
  //==========================================================================//
  uploadImage: function (req, res) {
    var userId = req.user.sub; //viene del Middleware de autenticacion

    var fileName = "Imagen no subida...";

    if (req.files) {
      var filePath = req.files.avatar.path;
      var fileSplit = filePath.split("/");
      var fileName = fileSplit[1];
      var extSplit = fileName.split(".");
      var fileExt = extSplit[1];

      if (
        fileExt == "png" ||
        fileExt == "jpg" ||
        fileExt == "jpeg" ||
        fileExt == "gif" ||
        fileExt == "pdf"
      ) {
        User.findByIdAndUpdate(
          userId,
          { avatar: fileName },
          { new: true },
          (err, userUpdated) => {
            if (err)
              return res
                .status(500)
                .send({ message: "La imagen no se ha subido" });

            if (!userUpdated)
              return res.status(404).send({
                message: "El usuario no existe y no se ha asignado la imagen",
              });

            return res.status(200).send({
              user: userUpdated,
            });
          }
        );
      } else {
        fs.unlink(filePath, (err) => {
          return res.status(200).send({ message: "La extensión no es válida" });
        });
      }
    } else {
      return res.status(200).send({
        message: fileName,
      });
    }
  },
  //==========================================================================//
  //====================== Obtener imagen de usuario =========================//
  //==========================================================================//
  getAvatargeFile: function (req, res) {
    var avatarFile = req.params.avatar;
    var path_file = "./uploads/" + avatarFile;

    fs.exists("./uploads/" + avatarFile, function (exists) {
      if (exists) {
        return res.sendFile(path.resolve(path_file));
      } else {
        return res.status(200).send({ message: "No existe la imagen....." });
      }
    });
  },
};

module.exports = controller;
