"use strict";
var mongoose = require("mongoose");
var app = require("./app");
var conf = require("./env.json");
var port = conf.dev.db.port;

mongoose.Promise = global.Promise;
mongoose
  .connect(conf.dev.db.conex)
  .then(() => {
    console.log("Conectado con exito....!!!");

    ///////////// Creacion de servicios  ////////////
    app.listen(port, () => {
      console.log(
        `Servidor corriendo correctamente en el puerto: localhost:${port}`
      );
    });
  })
  .catch((err) => console.log(err));
