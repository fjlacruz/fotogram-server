"use strict";

var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var PostSchema = Schema({
  created: Date,
  mensaje: { type: String },
  img: [
    {
      type: String,
    },
  ],
  coords: {
    type: String,
  },
  usuario: {
    type: Schema.ObjectId,
    ref: "User",
    required: [true, "Debe exisistir una relacion con un usuario"],
  },
});

PostSchema.pre("save", function (next) {
  this.created = new Date();
  next();
});

module.exports = mongoose.model("Post", PostSchema);
