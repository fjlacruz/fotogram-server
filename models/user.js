"use strict";

var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var UserSchema = Schema({
  nombre: String,
  email: String,
  password: String,
  avatar: { type: String, default: "av-1.png" },
  date: { type: Date, default: Date.now },
});

module.exports = mongoose.model("User", UserSchema);
