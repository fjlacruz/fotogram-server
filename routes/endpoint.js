"use strict";

var express = require("express");
var PostController = require("../controllers/post");
//var ImageController = require("../controllers/image");
var UserController = require("../controllers/user");

var router = express.Router();

//middelware para proteger rutas
var md_auth = require("../middelwares/autenticated");

var multipart = require("connect-multiparty");
var multipartMiddleware = multipart({ uploadDir: "./uploads" });
//============== Rutas de usuarios ================================//
router.post("/save-user", UserController.saveUser);
router.post("/login", UserController.loginUser);
router.get("/test", md_auth.ensureAuth, UserController.test);
router.put("/update-user", md_auth.ensureAuth, UserController.updateUser);
router.get("/user-list", UserController.getUsers);
router.post("/search-user", UserController.searchUser);
router.post(
  "/upload-image",
  multipartMiddleware,
  md_auth.ensureAuth,
  UserController.uploadImage
);
router.get("/get-user-avatar/:avatar", UserController.getAvatargeFile);
router.get("/user", md_auth.ensureAuth, UserController.getUser);

//============== Rutas de Post ================================//
router.post("/create-post", md_auth.ensureAuth, PostController.savePost);
router.get("/post-list", PostController.getListPosts);
router.get("/user-post-list", md_auth.ensureAuth, PostController.getUsersPosts);
// router.post(
//   "/upload-image-post",
//   multipartMiddleware,
//   md_auth.ensureAuth,
//   PostController.uploadImagePost
// );
router.get(
  "/get-post-img/:img",
  md_auth.ensureAuth,
  PostController.getPostImag
);


module.exports = router;
