newman

fotogram

❏ usuarios
↳ crear usuario
  POST http://localhost:3700/api/save-user [200 OK, 421B, 80ms]
  ✓  Status code is 200

↳ login de usuarios
  POST http://localhost:3700/api/login [201 Created, 670B, 351ms]
  ✓  Status code is 200
  ✓  Body matches string
  ✓  Status code name has string
  ✓  response must be valid and have a body
  ✓  Status code name has string

↳ Ruta de test
  GET http://localhost:3700/api/test [200 OK, 401B, 7ms]
  ✓  Response time is less than 200ms
  ✓  Status code is 200
  ✓  Body matches string

↳ Actualizacion de usuarios
  PUT http://localhost:3700/api/update-user [200 OK, 566B, 11ms]
  ✓  Status code is 200

↳ Buscar usuario
  POST http://localhost:3700/api/search-user [200 OK, 405B, 17ms]
  ✓  Estatus de la respuesta debe ser 200
  ✓  La respuesta debe ser un json valido
  ✓  La respuesta debe tener un cuerpo valido
  ✓  La respuesta en el cuerpo del json No debe tener errores
  ✓  La respuesta no puede ser vacia

↳ Subir imagen de usuario
  POST http://localhost:3700/api/upload-image [200 OK, 621B, 22ms]

↳ Buscar avatar de usuario
  GET http://localhost:3700/api/get-user-avatar/5VHzEs6O8_pDW0-fN-I74WwE.png [200 OK, 6.43KB, 9ms]
  ✓  Status code is 200

↳ Datos del usuario
  GET http://localhost:3700/api/user [200 OK, 532B, 7ms]
  ✓  Status code  200
  ✓  La respuesta debe ser un json valido
  ✓  La respuesta debe tener un cuerpo valido
  ✓  La respuesta en el cuerpo del json No debe tener errores
  ✓  El Body contiene los string de la comsulta
  ✓  La respuesta no puede ser vacia
  ✓  Content-Type cabeceras presente
  ✓  El tiempo de respuesta es menor que 200ms
  ┌
  │ {
  │   user: {
  │     sub: '5f2f4f39e17d0f57b3ee24d0',
  │     nombre: 'Javier',
  │     email: 'idsistemas15@gmail.com',
  │     avatar: '5VHzEs6O8_pDW0-fN-I74WwE.png',
  │     iat: 1597862309
  │   },
  │   ok: true
  │ }
  │ 'idsistemas15@gmail.com'
  │ 'string'
  └
  ✓  Test data type of the response
  ✓  Not failing

❏ Posts
↳ Crear Posts
  POST http://localhost:3700/api/create-post [200 OK, 710B, 16ms]

↳ Lista de todos los posts
  GET http://localhost:3700/api/post-list?pagina=2 [200 OK, 3.37KB, 16ms]

↳ Lista posts por usuario
  GET http://localhost:3700/api/user-post-list?pagina=1 [200 OK, 3.37KB, 12ms]

↳ Obtener imagen del post
  GET http://localhost:3700/api/get-post-img/HVe6_RmQD6yxE1iBHvQMeHHA.png [200 OK, 56.83KB, 6ms]

❏ test
↳ Lista posts por usuario Copy
  GET http://localhost:3700/api/user-post-list?pagina=1 [200 OK, 3.37KB, 10ms]
  ✓  Response time is less than 200ms
  ✓  Status code is 200

┌─────────────────────────┬───────────────────┬──────────────────┐
│                         │          executed │           failed │
├─────────────────────────┼───────────────────┼──────────────────┤
│              iterations │                 1 │                0 │
├─────────────────────────┼───────────────────┼──────────────────┤
│                requests │                13 │                0 │
├─────────────────────────┼───────────────────┼──────────────────┤
│            test-scripts │                22 │                0 │
├─────────────────────────┼───────────────────┼──────────────────┤
│      prerequest-scripts │                13 │                0 │
├─────────────────────────┼───────────────────┼──────────────────┤
│              assertions │                28 │                0 │
├─────────────────────────┴───────────────────┴──────────────────┤
│ total run duration: 1058ms                                     │
├────────────────────────────────────────────────────────────────┤
│ total data received: 72.75KB (approx)                          │
├────────────────────────────────────────────────────────────────┤
│ average response time: 43ms [min: 6ms, max: 351ms, s.d.: 90ms] │
└────────────────────────────────────────────────────────────────┘
